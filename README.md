# Project: Mystery Castle 

![setup][img01]

## Table of Contents 
- Overview
  - Terms
  - Design Goals
- Setup
  - Hardware
    - HP EliteBook
    - Raspberry Pi W/ WiFi
  - Uplink Node
    - Initial configuration
    - Configure NAT
    - Configuring the wireless interface
    - Configuring network interfaces
  - Mesh Gateway 
    - Install required software
    - Identify Interfaces
    - blacklist wireless interfaces from NetworkManager
    - Set up interfaces
  - Client Node
    - Install required software
    - Identify Interfaces
    - blacklist wireless interfaces from NetworkManager
    - Set up interfaces
- Troubleshooting
  - Can't set up wireless on ibss mode
  - batctl neighbors not appearing
  - Addressing 
  - Ubuntu not starting (4 minutes to boot)


## Overview

Project Phoenix hopes to bring the concept of decentralized community-driven
networking to the general public similar to projects like NYCMesh and Guifi Net.
To better illustrate the ultimate goal of the project, Project Phoenix has 
developed a simple demonstration of mesh technologies leveraging easy to acquire
equipment running free and open-source software. The end goal is simply
establishing a clear and practical example of how mesh technologies work for 
anyone interested in the project. 

For our demo, we've acquired four HP Elitebook 840s and one Raspberry pi 3 B+.
The HP's run a free and open-source OS along with the Raspberry Pi. Networking
is handled by standard protocols except on Layer 2 of the OSI model. To optimize
the network, we're using the B.A.T.M.A.N routing protocol which specializes in 
multi-hop mobile ad-hoc networking. As of Kernel version 2.6.38, the batman-adv
kernel module has been included with the Linux kernel.

One laptop acts
both as a mesh-client and a mesh-gateway. The mesh-gateway is responsible for
bridging the wireless "mesh" network to a mesh-uplink. The mesh-uplink is
responsible for managing logical addressing on the Network-Layer. It also is set
up as a masquerade to forward traffic between mesh nodes and outside networks. 

B.A.T.M.A.N Advanced was chosen because it addresses key issues with wireless
mesh networking. B.A.T.M.A.N Advanced is also shipped with the Linux kernel as
of 2.x. 

![Diagram][dgm01]

### Terms
  - Mesh uplink - Node responsible for routing traffic to the Internet on behalf 
                  of mesh nodes.

  - Mesh Gateway - Node responsible for announcing route to Internet

  - Mesh Client - Node running BATMAN-ADV and connected to the demo network. 

### Design Goals
Three foundational concepts are in mind when designing this demo.
  1) Portability
      - Transfer between venues should be simple. 
      - Internet Connectivity should be uncomplicated.
  2) Simplicity
      - The demo should not break if you breathe on it wrong. 
  3) Functionality
      - The concepts of a mesh network should be apparent, including demo
        software that illustrates mesh functionality while being exciting enough
        for general audiences. 

## Setup


### Hardware


#### HP EliteBook
Model: HP EliteBook 840 G2
OS: Ubuntu 18.04.1 LTS
Ethernet: I218-LM 
Wireless: Intel Wireless 3160 #83 

#### Raspberry Pi W/ wifi
Model: Raspberry Pi 3 Model B+
OS: Raspbian <insert version>

### Uplink Node
More often than not the demo will be presented at a location with wireless 
networking. Be sure to acquire the configuration details of the wireless network

  - Does the network use a captive portal to log in? 
  - Does the pi support the network infrastructure (802.11bgn vs 802.11ax etc.)?

###### Initial configuration

For this demo, Raspbian was chosen to run on the Raspberry pi. After installing
the OS, run: 
```
# raspi-config
```
- Connect to a wireless network to patch the system. 

The following third party software is required:
  - dnsmasq
  - iptables
  - wireless-tools

###### Configure NAT
To enable IP packet forwarding, edit /etc/sysctl.conf. 

  - Uncomment net.ipv4.ip_forward=1

The following commands configure NAT using iptables. 
```
$ sudo iptables -A POSTROUTING \
-o wlan0 \
-j MASQUERADE 

$ sudo iptables -A FORWARD \
-i wlan0 \
-o eth0 \
-m state \
--state RELATED,ESTABLISHED \
-j ACCEPT 

$ sudo iptables -A FORWARD \
-i eth0 \
-o wlan0 \
-j ACCEPT \

$ sudo iptables-save > /etc/iptables.rules
```

###### Configuring the wireless interface
This configuration enables roaming between different SSIDs.

Edit /etc/wpa_suppliciant/wpa_suppliciant.conf

For each desired wlan, add the following;
```
network={       
  ssid="foonet"
  psk="barpass"  
  id_str="wlan1" 
  priority=1
}
```

###### Configuring network interfaces

edit /etc/network/interfaces


```
# Configure wlan
allow-hotplug wlan0
iface wlan0 inet manual
  wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf

iface wlan1 inet dhcp

# Configure eth0
auto eth0
iface eth0 inet static
  address 10.27.0.1
  netmask 255.255.255.0
  post-up iptables-restore < /etc/iptables.rules
```

For the mesh network, ip addressing is currently handled by the uplink node.
In the future IP addressing will be handled by another means. 

The 10.27.0.0/24 subnet is allocated for the mesh IBSS. The IBSS is configured
and managed by the mesh-gateway. 

### Mesh Gateway

###### Install required software

```
$ sudo apt-get install batctl bridge-tools
```

###### Identify Interfaces

Identify interfaces for configuration
```
$ sudo ip link 
...<Truncated>

2: enp0s25: <NO-CARRIER, BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>

3: wlo1: <BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>

...<Truncated>
```

###### blacklist wireless interfaces from NetworkManager

Edit /etc/NetworkManager/NetworkManager.conf
```
[keyfile]
unmanaged-devices=mac:< wlo1 link/ether address >
```

After reloading the network, NetworkManager will no-longer manage the WLAN
interface. Without this configuration, NM will over-rule any configurations you
set manually in /etc/network/interfaces. 

###### Set up interfaces

Configure the LAN access port as shown: 
```
auto enp0s25
iface enp0s25 inet manual
```

For the wireless interface, we'll be using an easy to remember alias. 
```
auto wlan0
iface wlan0 inet manual
  pre-up iw phy phy0 interface add wlan0 type ibss
  wireless-essid mesh
  wireless-mode ad-hoc
  wireless-freq 2412
  # We add 32 additional bits to fit the B.A.T.M.A.N datagram headers.
  mtu 1532
```

B.A.T.M.A.N uses the bat0 interface for TX/RX. wlan0 is **not** used for logical
routing. 
```
auto bat0
iface bat0 inet manual
  pre-up batctl if add wlan0
  post-up batctl gw_mode server
  pre-down batctl if del wlan0
```

Finally we configure a bridge between the LAN and WLAN adapters.
```
auto br0
iface br0 inet dhcp
  bridge_ports enp0s25 bat0
```

For now, the prototype uses a contemporary logical addressing service. DHCP
works, but the next version of this prototype will use a decenteralized
addressing scheme like CJDNS.

### Client Node

###### Install required software

```
$ sudo apt-get install batctl 
```

###### Identify Interfaces

Identify interfaces for configuration
```
$ sudo ip link 
...<Truncated>

2: enp0s25: <NO-CARRIER, BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>

3: wlo1: <BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>
```

###### blacklist wireless interfaces from NetworkManager

Edit /etc/NetworkManager/NetworkManager.conf
```
[keyfile]
unmanaged-devices=mac:< wlo1 link/ether address >
```

###### Set up interfaces

Configure the LAN access port as shown: 
```
auto enp0s25
iface enp0s25 inet manual
```

For the wireless interface, we'll be using an easy to remember alias. 
```
auto wlan0
iface wlan0 inet manual
  pre-up iw phy phy0 interface add wlan0 type ibss
  wireless-essid mesh
  wireless-mode ad-hoc
  wireless-freq 2412
  # We add 32 additional bits to fit the B.A.T.M.A.N datagram headers.
  mtu 1532
```

B.A.T.M.A.N uses the bat0 interface for TX/RX. wlan0 is **not** used for logical
routing. 
```
auto bat0
iface bat0 inet dhcp
  pre-up batctl if add wlan0
  post-up batctl gw_mode server
  pre-down batctl if del wlan0
```

### Troubleshooting
Incoming...

[dgm01]: img/mc-diagram.png
[img01]: img/Scaled.png
